package days

class Day11(rawInput: List<String>) : Day {
    private val input = rawInput[0].toInt()
    private val grid = Grid(300, input)

    override fun part1() {
        val largestSquare = grid.getLargestSquare(3)
        println(largestSquare)
    }

    override fun part2() {
        println(grid.getLargestSquareAndSize())
    }

    class Grid(private val size: Int, private val gridSerialNumber: Int) {
        private val grid = Array(size) { x -> Array(size) { y -> FuelCell(x + 1, y + 1, gridSerialNumber) } }
        fun getLargestSquare(squareSide: Int): Pair<Pair<Int, Int>, Int> {
            val largest = (0 until size - squareSide - 1).flatMap { x ->
                (0 until size - squareSide - 1).map { y ->
                    val sum = (0 until squareSide).flatMap { i -> (0 until squareSide).map { j -> grid[x + i][y + j].powerLevel } }.sum()
                    Triple(x + 1, y + 1, sum)
                }
            }.maxBy { it.third }!!
            return Pair(Pair(largest.first, largest.second), largest.third)
        }

        fun getLargestSquareAndSize(): Triple<Pair<Int, Int>, Int, Int> {
            val largest = (1 until size).map {
                val square = getLargestSquare(it)

                Triple(square.first, it, square.second)
            }.maxBy { it.third }!!
            return largest
        }
    }

    class FuelCell(x: Int, y: Int, gridSerialNumber: Int) {
        private val rackId = x + 10
        val powerLevel = ((((rackId * y) + gridSerialNumber) * rackId / 100) % 10) - 5
    }
}